import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

import pandas as pd
import matplotlib.pyplot as plt






###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os



def calculate_dR(photon_candidate):
     
    photon_reco = ROOT.TLorentzVector()
    photon_reco.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    photon_gen = ROOT.TLorentzVector()
    photon_gen.SetPtEtaPhiE(photon_candidate.genParticle().pt(), photon_candidate.genParticle().eta(), photon_candidate.genParticle().phi(), photon_candidate.genParticle().energy())
     
    return photon_reco.DeltaR(photon_gen)


def hadronic_fake_candidate(photon_candidate, genJets, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    photon_vector = ROOT.TLorentzVector()
    photon_vector.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", photon_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    jet_around_photon = False
    for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
        genJet_vector = ROOT.TLorentzVector()
        genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

        DeltaR = photon_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
        if DeltaR < 0.3:
            jet_around_photon = True

    is_prompt = False
    pdgId = 0
    pt_fake = float(99999)

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = photon_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.1:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            pt_fake = genParticle.pt()

    fakept_by_recopt = pt_fake/photon_candidate.pt()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if jet_around_photon and not (prompt_electron or prompt_photon or prompt_muon) and fakept_by_recopt<1.2 and fakept_by_recopt>0.8:
        return [1, pt_fake, min_DeltaR]
    else:
        return [0, None, None]








xlable_keys = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "photonIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "relphotonIso": [],
    "hadronicOverEm": [],
    "pt": [],
    "eta": [],
    "phi": [],
    "subdetId": [],
    "genpt": [],
    "dR": [],
    "is_real": [],
    "pt_by_genpt": [],
    "trkIso": [],
    "ecalIso": [],
    "hcalIso": []

    }







photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"
#RecHitHandle, RecHitLabel = Handle("edm::SortedCollection<EcalRecHit,edm::StrictWeakOrdering<EcalRecHit> >"), "reducedEgamma:reducedEBRecHits" 
#genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
#genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"
#simTrackHandle, simTrackLabel = Handle("std::vector<SimTrack>"), "g4SimHits"




#path_GJet_files = "/eos/user/n/nkasarag/PhD/compare_JGet_DY_photon_objects/GJet_Run3_MiniAOD/Pt-40toInf/GJet_PT-40_DoubleEMEnriched_MGG-80_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/230828_104237/0000/"


#filenames=["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/ZToEE_TuneCUETP8M1_14TeV-pythia8/ZToEE_TuneCUETP8M1_14TeV/231023_171652/0000/output_1.root",
#"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/QCD_Pt-15to7000_TuneCUETP8M1_Flat_14TeV-pythia8/QCD_Pt-15to7000_TuneCUETP8M1/231023_171515/0000/output_1.root",
#"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/TT_TuneCP5_14TeV-powheg-pythia8/TT_TuneCP5_14TeV/231023_171611/0000/output_1.root"]


filenames = ["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_1/231024_084713/0000/output_1.root"]

"""filenames = ["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_1/231024_084713/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_2/231024_085617/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_2/231024_085617/0000/output_2.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZEE_14/RelValZEE_14_1/231024_085636/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZEE_14/RelValZEE_14_2/231024_085645/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZMM_14/RelValZMM_14_1/231024_085704/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZMM_14/RelValZMM_14_2/231024_085711/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZTT_14/RelValZTT_14_1/231024_085720/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZTT_14/RelValZTT_14_2/231024_085741/0000/output_1.root"]"""

#filenames = os.listdir(path_GJet_files)
    # just take one file for tests
#filenames = filenames[:30]

file_num = 0

is_real = 0
num_photon = 0
prompt_photon_pt = []

for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(filename)

    eve_num = 0  

    stop_index = -100
    for i,event in enumerate(events):

            if i == stop_index: 
                break 
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            #a = EventAuxiliary()
            #event.to(a)

            eve_num+=1


            #print(event.eventAuxiliary().id().event())

            #event.getByLabel(photonLabel, photonHandle)
            #event.getByLabel(genParticlesLabel, genParticlesHandle)
            

            #genParticles = genParticlesHandle.product()

                
                
print(eve_num)


#runNumber = eventId.run();
#eventNumber = eventId.event();
#bunchCrossingId = eventId.bunchCrossing();
            


    
#            for photon in photonHandle.product():
#
#                num_photon += 1
#
#                #if photon.pt() < 20: continue
#                
#
#                try:
#                    pdgId = photon.genParticle().pdgId()
#                    if pdgId == 22:
#                        is_real += 1
#                        prompt_photon_pt.append(photon.pt())
#            
#                except ReferenceError:
#                    pass
#                    


#print("num_photons: ", num_photon)
#print("num_prompt_photons: ", is_real)
#print("len(prompt_photon_pt): ",len(prompt_photon_pt))
#
#prompt_photon_pt = np.array(prompt_photon_pt)
##print(prompt_photon_pt)
#np.save("old_samples_prompt_photon_pt", prompt_photon_pt)
#
#plt.hist(prompt_photon_pt, bins=100)
#plt.xlabel("pT [GeV]")
#plt.ylabel("Number of entries")
#plt.yscale("log")
#plt.xlim([0,500])
#plt.savefig("old_samples_prompt_photon_pt.png")


