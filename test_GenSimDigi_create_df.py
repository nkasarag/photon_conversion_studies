import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

import pandas as pd
import matplotlib.pyplot as plt






###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os










#photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"
#RecHitHandle, RecHitLabel = Handle("edm::SortedCollection<EcalRecHit,edm::StrictWeakOrdering<EcalRecHit> >"), "reducedEgamma:reducedEBRecHits" 
#genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
#genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"
simTrackHandle, simTrackLabel = Handle("std::vector<SimTrack>"), "g4SimHits"
simVertexHandle, simVertexLabel = Handle("std::vector<SimVertex>"), "g4SimHits"

photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"




#path_GJet_files = "/eos/user/n/nkasarag/PhD/compare_JGet_DY_photon_objects/GJet_Run3_MiniAOD/Pt-40toInf/GJet_PT-40_DoubleEMEnriched_MGG-80_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/230828_104237/0000/"


#filenames=["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/ZToEE_TuneCUETP8M1_14TeV-pythia8/ZToEE_TuneCUETP8M1_14TeV/231023_171652/0000/output_1.root",
#"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/QCD_Pt-15to7000_TuneCUETP8M1_Flat_14TeV-pythia8/QCD_Pt-15to7000_TuneCUETP8M1/231023_171515/0000/output_1.root",
#"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/TT_TuneCP5_14TeV-powheg-pythia8/TT_TuneCP5_14TeV/231023_171611/0000/output_1.root"]


#filenames = ["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion_new/RelValTTbar_14TeV/RelValZTT_14_1_GEN-SIM-DIGI-RAW/231128_095807/0000/output_27.root"]
filenames = ["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion_new/RelValTTbar_14TeV/RelValZTT_14_2_GEN-SIM-DIGI-RAW/231128_131948/0000/output_29.root"]

"""filenames = ["/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_1/231024_084713/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_2/231024_085617/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValTTbar_14TeV/RelValTTbar_14TeV_2/231024_085617/0000/output_2.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZEE_14/RelValZEE_14_1/231024_085636/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZEE_14/RelValZEE_14_2/231024_085645/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZMM_14/RelValZMM_14_1/231024_085704/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZMM_14/RelValZMM_14_2/231024_085711/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZTT_14/RelValZTT_14_1/231024_085720/0000/output_1.root",
"/eos/cms/store/group/phys_egamma/ec/nkasarag/photons_conversion/RelValZTT_14/RelValZTT_14_2/231024_085741/0000/output_1.root"]"""

#filenames = os.listdir(path_GJet_files)
    # just take one file for tests
#filenames = filenames[:30]

file_num = 0




photon_info = {
     "photon_pt": [],
     "photon_eta": [],
     "photon_phi": [],
     "photon_vrtx_r": [],
     "photon_vrtx_z": [],
     "num_ele": [],
     "ele_charge_1": [],
     "ele_pt_1": [],
     "ele_eta_1": [],
     "ele_phi_1": [],
     "ele_charge_2": [],
     "ele_pt_2": [],
     "ele_eta_2": [],
     "ele_phi_2": [],
     "vrtx_r_1": [],
     "vrtx_z_1": [],
     "vrtx_r_2": [],
     "vrtx_z_2": []
}


for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(filename)

    stop_index = -1000
    eve_num = 0 

    for i,event in enumerate(events):
            

            if i == stop_index: 
                break 
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            event.getByLabel(simTrackLabel, simTrackHandle)
            event.getByLabel(simVertexLabel, simVertexHandle)
            
            sim_track = simTrackHandle.product()
            sim_vertex = simVertexHandle.product()

            
            info_to_store = {}

            """
            check these for few of the details->
            TWiki: https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideMCTruth
            SimTrack.h: https://github.com/cms-sw/cmssw/blob/7d99d3006f443078e47ff140f5ee7d4962298135/SimDataFormats/Track/interface/SimTrack.h#L9
            SimVertex.h: https://github.com/cms-sw/cmssw/blob/7d99d3006f443078e47ff140f5ee7d4962298135/SimDataFormats/Vertex/interface/SimVertex.h#L5

            """

            #print("\t getting primary photons track index...")

            for trk in sim_track:

                vrtx = sim_vertex[trk.vertIndex()]
                trk_pt = trk.momentum().Pt()

                if (trk.type() == 22) and (vrtx.parentIndex()==-1) and (trk_pt>20):

                    info_to_store[str(trk.trackId())] = [[trk.momentum(), vrtx.position()]]

                    eve_num += 1

            

            #print(info_to_store)
            #print("\t getting electron and vertex info...")

            primary_photon_trackId = info_to_store.keys()
            for trk in sim_track:
                vrtx = sim_vertex[trk.vertIndex()]
                if (trk.type() == -11) or (trk.type() == 11):

                    parInd = str(vrtx.parentIndex())
                    if (parInd in primary_photon_trackId) and ((vrtx.processType() == 14) or (vrtx.processType() == 4)):
                        info_to_store[parInd].append([trk.charge(), trk.momentum(), vrtx.position()])

                    

            #print("\t preparing info to save...")

            for key in info_to_store:
                    #print(key)
                #if len(info_to_store[key])==1: continue
#
                #if len(info_to_store[key])==2:
                #    photon_info["photon_pt"].append(info_to_store[key][0].Pt())
                #    photon_info["photon_eta"].append(info_to_store[key][0].Eta())
                #    photon_info["photon_phi"].append(info_to_store[key][0].Phi())
#
                #    photon_info["num_ele"].append(1)
                #    photon_info["ele_charge"].append([info_to_store[key][1][0]])
                #    photon_info["ele_pt"].append([info_to_store[key][1][1].Pt()])
                #    photon_info["ele_eta"].append([info_to_store[key][1][1].Eta()])
                #    photon_info["ele_phi"].append([info_to_store[key][1][1].Phi()])
#
                #    photon_info["vrtx_r"].append( [np.sqrt(info_to_store[key][1][2].x()**2 + info_to_store[key][1][2].y()**2)] )
                #    photon_info["vrtx_z"].append([info_to_store[key][1][2].z()])

                if len(info_to_store[key])>1:
                    photon_info["photon_pt"].append(info_to_store[key][0][0].Pt())
                    photon_info["photon_eta"].append(info_to_store[key][0][0].Eta())
                    photon_info["photon_phi"].append(info_to_store[key][0][0].Phi())

                    photon_info["photon_vrtx_r"].append(np.sqrt(info_to_store[key][0][1].x()**2 + info_to_store[key][0][1].y()**2))
                    photon_info["photon_vrtx_z"].append(info_to_store[key][0][1].z())

                    photon_info["num_ele"].append(2)
                    photon_info["ele_charge_1"].append(info_to_store[key][1][0])
                    photon_info["ele_pt_1"].append(info_to_store[key][1][1].Pt())
                    photon_info["ele_eta_1"].append(info_to_store[key][1][1].Eta())
                    photon_info["ele_phi_1"].append(info_to_store[key][1][1].Eta())

                    photon_info["ele_charge_2"].append(info_to_store[key][2][0])
                    photon_info["ele_pt_2"].append(info_to_store[key][2][1].Pt())
                    photon_info["ele_eta_2"].append(info_to_store[key][2][1].Eta())
                    photon_info["ele_phi_2"].append(info_to_store[key][2][1].Phi())

                    #e1 = ROOT.TLorentzVector()
                    #e1.SetPtEtaPhiE(info_to_store[key][1][1].Pt(), info_to_store[key][1][1].Eta(), info_to_store[key][1][1].Eta(), info_to_store[key][1][1].E())
#
                    #e2 = ROOT.TLorentzVector()
                    #e2.SetPtEtaPhiE(info_to_store[key][2][1].Pt(), info_to_store[key][2][1].Eta(), info_to_store[key][2][1].Eta(), info_to_store[key][2][1].E())
#
                    #print("m_ee: ", (e1+e2).M())

                    #print(np.sqrt(info_to_store[key][1][2].x()**2 + info_to_store[key][1][2].y()**2))
                    #print(info_to_store[key][1][2].z())
                    photon_info["vrtx_r_1"].append( np.sqrt(info_to_store[key][1][2].x()**2 + info_to_store[key][1][2].y()**2) )
                    photon_info["vrtx_z_1"].append(info_to_store[key][1][2].z())

                    photon_info["vrtx_r_2"].append( np.sqrt(info_to_store[key][2][2].x()**2 + info_to_store[key][2][2].y()**2) )
                    photon_info["vrtx_z_2"].append(info_to_store[key][2][2].z())

                #if len(info_to_store[key])>3: print("samething wrong")





print("\t saving dataframe...")

df = pd.DataFrame(photon_info)
print("df.vrtx_z_2.values.size: ", df.vrtx_z_2.values.size)
#print(df["ele_pt"])#.head())
print(df.tail())

path = "/eos/user/n/nkasarag/photon_conversion_studies/"
filename = "photon_conversion_studies"

df.to_pickle("%s%s.pickle"%(path, filename))

                    #e_info.append([vrtx.parentIndex(), vrtx.processType(), trk.charge()])#, [vrtx.position().x(), vrtx.position().y(),vrtx.position().z()], [trk.momentum().Px(), trk.momentum().Py(), trk.momentum().Py(), trk.momentum().E()]])
                    #if len(e_info)>2: break






            #for vrtx in sim_vertex:
            #    print(vrtx.parentIndex())
        #
            #for trk in sim_track:
#
#
            #    if (trk.type()==22):
            #        print("sim_vertex[trk.vertIndex()].parentIndex():", sim_vertex[trk.vertIndex()].parentIndex())
                #if (trk.type()==11) or (trk.type()==-11) or (trk.type()==22):
#
            #        print("sim_vertex[trk.vertIndex()].eventId().event():", sim_vertex[trk.vertIndex()].eventId().event())
            #    #if trk.type()==22:
#
            #        print("vertIndex: ", trk.vertIndex())
            #        print("noVertex: ", trk.noVertex())
            #        print("genpartIndex: ", trk.genpartIndex())
            #        print("noGenpart: ", trk.noGenpart())
            #        #print("trackerSurfacePosition: ", trk.trackerSurfacePosition())
            #        print("trackerSurfaceMomentum: ", trk.trackerSurfaceMomentum())
            #        print("crossedBoundary: ", trk.crossedBoundary())
            #        print("getPositionAtBoundary: ", trk.getPositionAtBoundary())
            #        print("getMomentumAtBoundary: ", trk.getMomentumAtBoundary())
            #        print("getIDAtBoundary: ", trk.getIDAtBoundary())
#
            #        print("eventId().event(): ", trk.eventId().event()) 
            #        print("trackId: ", trk.trackId()) 
            #        print("type: ", trk.type()) 
            #        print("momentum: ", trk.momentum()) 
            #        print("charge: ", trk.charge()) 
#
            #        print("\n")
                



#runNumber = eventId.run();
#eventNumber = eventId.event();
#bunchCrossingId = eventId.bunchCrossing();
            


    
#            for photon in photonHandle.product():
#
#                num_photon += 1
#
#                #if photon.pt() < 20: continue
#                
#
#                try:
#                    pdgId = photon.genParticle().pdgId()
#                    if pdgId == 22:
#                        is_real += 1
#                        prompt_photon_pt.append(photon.pt())
#            
#                except ReferenceError:
#                    pass
#                    


#print("num_photons: ", num_photon)
#print("num_prompt_photons: ", is_real)
#print("len(prompt_photon_pt): ",len(prompt_photon_pt))
#
#prompt_photon_pt = np.array(prompt_photon_pt)
##print(prompt_photon_pt)
#np.save("old_samples_prompt_photon_pt", prompt_photon_pt)
#
#plt.hist(prompt_photon_pt, bins=100)
#plt.xlabel("pT [GeV]")
#plt.ylabel("Number of entries")
#plt.yscale("log")
#plt.xlim([0,500])
#plt.savefig("old_samples_prompt_photon_pt.png")


