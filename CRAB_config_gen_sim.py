from CRABClient.UserUtilities import config

config = config()

key = 'RelValZTT_14_2_GEN-SIM-DIGI-RAW'

config.General.requestName = key
config.General.workArea = 'crab_projects'
config.General.transferOutputs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'slim_MiniAODs_gen_sim.py'


#config.Data.inputDataset = '/QCD_Pt-15to7000_TuneCUETP8M1_Flat_14TeV-pythia8/Run3Winter21DRMiniAOD-FlatPU20to70_for_DNN_112X_mcRun3_2021_realistic_v16_ext1-v2/MINIAODSIM' 
#config.Data.inputDataset = '/TT_TuneCP5_14TeV-powheg-pythia8/Run3Winter21DRMiniAOD-FlatPU20to70_for_DNN_112X_mcRun3_2021_realistic_v16_ext1-v2/MINIAODSIM'
#config.Data.inputDataset = '/ZToEE_TuneCUETP8M1_14TeV-pythia8/Run3Winter21DRMiniAOD-FlatPU20to70_for_DNN_112X_mcRun3_2021_realistic_v16_ext1-v2/MINIAODSIM'

"""dataset = {
    "RelValTTbar_14TeV_1": '/RelValTTbar_14TeV/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV204-v1/MINIAODSIM',
    "RelValTTbar_14TeV_2": '/RelValTTbar_14TeV/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV204-v2/MINIAODSIM',
    "RelValZEE_14_1": '/RelValZEE_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v1/MINIAODSIM',
    "RelValZEE_14_2": '/RelValZEE_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v2/MINIAODSIM',
    "RelValZMM_14_1": '/RelValZMM_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v1/MINIAODSIM',
    "RelValZMM_14_2": '/RelValZMM_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v2/MINIAODSIM',
    "RelValZTT_14_1": '/RelValZTT_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v1/MINIAODSIM',
    "RelValZTT_14_2": '/RelValZTT_14/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV208-v2/MINIAODSIM'
}"""

#dataset = "/RelValTTbar_14TeV/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV204-v1/GEN-SIM-DIGI-RAW"
dataset = "/RelValTTbar_14TeV/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV204-v2/GEN-SIM-DIGI-RAW"
#dataset = "/RelValTTbar_14TeV/CMSSW_13_0_11-PU_130X_mcRun3_2023_realistic_relvals2023D_v1_RV204-v1/MINIAODSIM"

#config.Data.inputDataset = dataset[key]
config.Data.inputDataset = dataset

config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 10
config.Data.publication = False 
config.Data.allowNonValidInputDataset  = True
config.Data.outputDatasetTag = key

config.Data.outLFNDirBase = "/store/group/phys_egamma/ec/nkasarag/photons_conversion_new/"  #points to /eos/cms/store/group/phys_egamma/...
config.Data.publication = False
config.Site.storageSite = 'T2_CH_CERN'
config.Site.ignoreGlobalBlacklist = True
