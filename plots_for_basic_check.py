from unittest import TextTestRunner
import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
#hep.style.use(hep.style.ROOT)
hep.style.use("CMS")
import os
from hist import Hist

import pandas as pd
import shutil


def plot_hist(lst1, lst3, w1, w3, leg1, leg3, xlable, ylable, outpath, eta_range, pt_range):

    #nbins=50

    range_keys = {
    "energyRaw": [0, 400, 80],
    "energyRaw": [0, 200 if eta_range[1]<1.5 else 400, 80],
    #"r9":  [0.1, 1.2, 80]3
    "r9":  [0.3, 1.2, 80],
    "sieie": [0, 0.02 if eta_range[1]<1.5 else 0.05, 80],
    "etaWidth": [0, 0.25, 40],
    "phiWidth": [0, 0.5, 40],
    "hoe": [0, 0.5, 40],
    "ecalPFClusterIso": [0, 15, 40],
    "trkSumPtHollowConeDR03": [0, 20, 40],
    "trkSumPtSolidConeDR04": [0, 20, 40],
    "pfChargedIso": [0, 50, 40],
    "hcalPFClusterIso": [0, 15, 40],
    "pt": [20, 200, 25],#[pt_range[0], pt_range[1], 50],
    "eta": [-2.5, 2.5, 20],
    "phi": [-3.2, 3.2, 8],
    }

    rmin = range_keys[xlable][0]
    rmax = range_keys[xlable][1]
    nbins = range_keys[xlable][2]



    hist1, bins1 = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=w1)
    hist3, bins3 = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=w3)

    hist1 = hist1/(np.sum(hist1)*(bins1[1]-bins1[0]))
    hist3 = hist3/(np.sum(hist3)*(bins3[1]-bins3[0]))

    #print("w1: ", w1)
    #print("w3: ", w3)
    hist1_err, _ = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=(w1*w1))
    hist3_err, _ = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=(w3*w3))
#   
    #print("hist1_err: ", hist1_err)
    #print("hist2_err: ", hist3_err)
    hist1_err = np.sqrt(hist1_err)
    hist3_err = np.sqrt(hist3_err)
    #print("hist1_err)1/2: ", hist1_err)
    #print("hist2_err)1/2: ", hist3_err)


    
    
    #fig, ax = plt.subplots(figsize=(10, 7))
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]}, sharex=True)
    hep.cms.label(data=False, year=2018, com=13)
    hep.histplot(
        hist1,
        bins=bins1,
        yerr = hist1_err,
        #histtype="errorbar",
        color="black",
        alpha=1,
        #edgecolor="black",
        linewidth=2,
        label=leg1,
        ax=ax[0],
        #density=True
    )

    hep.histplot(
        hist3,
        bins=bins3,
        yerr = hist3_err,
        #histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        linewidth=2,
        linestyle="--",
        label=leg3,
        ax=ax[0],
        #density=True
    )

    ratio = np.nan_to_num(hist3/hist1)
    ratio_err = np.nan_to_num(ratio*np.sqrt((hist3_err/hist3)**2+(hist1_err/hist1)**2))

    hep.histplot(
        ratio,
        bins=bins3,
        yerr = ratio_err,
        histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        #linestyle="--",
        ax=ax[1],
    )

    

    region = "eta"+str(eta_range) + "__pt"+str(pt_range)
    region = region.replace(" ","")
    region = region.replace(".","p")
    region= region.replace("[","_")
    region = region.replace("]","")
    region = region.replace(",","to")
    #print(region)

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", outpath+"res")

    sub_folder = outpath+"1D_dist/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", path_to_save+"res")

 

    
    ax[0].set_ylim(0, 1.25*ax[0].get_ylim()[1])
    ax[0].set_ylabel(ylable, fontsize=22)

    
    ax[1].set_xlim((rmin, rmax))
    ax[1].set_ylim((0.5, 1.5))


    if eta_range[1]<1.5: ax[1].set_xlabel(xlable_keys[xlable]+" (EB)", fontsize=22)
    else: ax[1].set_xlabel(xlable_keys[xlable]+" (EE)", fontsize=22)
    ax[1].set_xlabel("Ratio")

    

    t = r"%s$\leq$|$\eta$|<%s    %s GeV$\leq$$p_T$ <%s GeV"%(eta_range[0],eta_range[1],pt_range[0],pt_range[1])

    if pt_range[1]==999999: t = r"%s$\leq$|$\eta$|<%s    $p_T\geq$%s GeV"%(eta_range[0],eta_range[1],pt_range[0])
    
    #ax.set_title(t)
    
    ax[0].legend(fontsize=22)

    
    plt.tight_layout()
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()



df = pd.read_pickle("/eos/user/n/nkasarag/photon_conversion_studies/photon_conversion_studies.pickle")




plt.hist(df.photon_pt.values, bins=100)
plt.tight_layout()
plt.xlabel("pT [GeV]")
plt.savefig("test_plots/pt.png")
plt.clf()

plt.hist(df.photon_eta.values, bins=100)
plt.tight_layout()
plt.xlabel("eta")
plt.savefig("test_plots/eta.png")
plt.clf()

plt.hist(df.photon_phi.values, bins=100)
plt.tight_layout()
plt.xlabel("phi [rad]")
plt.savefig("test_plots/phi.png")
plt.clf()


plt.hist2d(df.vrtx_z_1, df.vrtx_r_1, bins=100, cmin=0.5)
plt.tight_layout()
plt.title("Vertex of photon conversion")
plt.xlabel("z [cm]")
plt.ylabel("r [cm]")
plt.savefig("test_plots/conversion_vrtx_r_vs_z.png")
plt.clf()

plt.hist2d(df.photon_vrtx_z, df.photon_vrtx_r, bins=100, cmin=0.5)
plt.tight_layout()

plt.xlabel("z [cm]")
plt.ylabel("r [cm]")
plt.savefig("test_plots/photon_production_vrtx_r_vs_z.png")
plt.clf()










