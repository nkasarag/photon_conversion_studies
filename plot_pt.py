import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

import pandas as pd
import matplotlib.pyplot as plt






###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os



def calculate_dR(photon_candidate):
     
    photon_reco = ROOT.TLorentzVector()
    photon_reco.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    photon_gen = ROOT.TLorentzVector()
    photon_gen.SetPtEtaPhiE(photon_candidate.genParticle().pt(), photon_candidate.genParticle().eta(), photon_candidate.genParticle().phi(), photon_candidate.genParticle().energy())
     
    return photon_reco.DeltaR(photon_gen)


def hadronic_fake_candidate(photon_candidate, genJets, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    photon_vector = ROOT.TLorentzVector()
    photon_vector.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", photon_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    jet_around_photon = False
    for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
        genJet_vector = ROOT.TLorentzVector()
        genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

        DeltaR = photon_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
        if DeltaR < 0.3:
            jet_around_photon = True

    is_prompt = False
    pdgId = 0
    pt_fake = float(99999)

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = photon_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.1:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            pt_fake = genParticle.pt()

    fakept_by_recopt = pt_fake/photon_candidate.pt()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if jet_around_photon and not (prompt_electron or prompt_photon or prompt_muon) and fakept_by_recopt<1.2 and fakept_by_recopt>0.8:
        return [1, pt_fake, min_DeltaR]
    else:
        return [0, None, None]








prompt_photon_pt_old = np.load("old_samples_prompt_photon_pt.npy")
prompt_photon_pt_relVal = np.load("relVal_samples_prompt_photon_pt.npy")



print("len(prompt_photon_pt): ",len(prompt_photon_pt_old))
print("len(prompt_photon_pt): ",len(prompt_photon_pt_relVal))




plt.hist(prompt_photon_pt_old, bins=200)
plt.xlabel("pT [GeV]")
plt.ylabel("Number of entries")
plt.yscale("log")
plt.xlim([0,500])
plt.savefig("old_samples_prompt_photon_pt.png")
plt.clf()

plt.hist(prompt_photon_pt_relVal, bins=100)
plt.xlabel("pT [GeV]")
plt.ylabel("Number of entries")
plt.yscale("log")
plt.xlim([0,500])
plt.savefig("relVal_samples_prompt_photon_pt.png")


